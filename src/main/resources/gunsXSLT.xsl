<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
    <html>
        <body style="font-family: Arial;
                     font-size: 12pt;
                     background-color: #f59042;">
            <div style="background-color: green;
                        color:black;">
                <h1>Guns</h1>
            </div>
            <table border="7">
                <tr bgcolor="#9acd32">
                    <th>Model</th>
                    <th>Handy</th>
                    <th>Origin</th>
                    <th>Material</th>
                    <th>TTC</th>
                </tr>
                <xsl:for-each select="guns/gun">
                    <tr>
                        <td><xsl:value-of select="modelName"/></td>
                        <td><xsl:value-of select="handy"/> hand(s)</td>
                        <td><xsl:value-of select="origin"/></td>
                        <td><xsl:value-of select="material"/></td>
                        <td>
                            Range: <xsl:value-of select="ttc/range"/> metres;
                            Scope: <xsl:value-of select="ttc/scope"/> metres;
                            Clip: <xsl:value-of select="ttc/clip"/>;
                            Optic: <xsl:value-of select="ttc/optic"/>;
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </body>
    </html>
    </xsl:template>
</xsl:stylesheet>
