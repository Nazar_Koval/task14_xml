package com.koval.parser.sax;

import com.koval.model.gun.Gun;
import com.koval.writer.Writer;
import org.apache.logging.log4j.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import static com.koval.validator.Validator.validate;

public class SaxParser {

    private static Logger logger = LogManager.getLogger(SaxParser.class);
    private static ArrayList<Gun> guns = new ArrayList<>();

    public SaxParser()throws ParserConfigurationException, SAXException, IOException {

        if(validate("src/main/resources/gunsXSD.xsd",
                "src/main/resources/guns.xml")){
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            SAXParser saxParser = saxParserFactory.newSAXParser();
            XmlHandler xmlHandler = new XmlHandler();
            saxParser.parse("src/main/resources/guns.xml",xmlHandler);

        }else logger.trace("Failure during validation");

    }

    private static class XmlHandler extends DefaultHandler {

        private String helper;
        private String modelName;
        private Integer handy;
        private String origin;
        private String material;
        private Integer range;
        private Integer scope;
        private Boolean clip;
        private Boolean optic;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            helper = qName;
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String info = new String(ch,start,length);
            info = info.replace("\n","").trim();

            if(!info.isEmpty()){
                switch (helper) {
                    case "modelName":
                        modelName = info;
                        break;
                    case "handy":
                        handy = Integer.parseInt(info);
                        break;
                    case "origin":
                        origin = info;
                        break;
                    case "material":
                        material = info;
                        break;
                    case "range":
                        range = Integer.parseInt(info);
                        break;
                    case "scope":
                        scope = Integer.parseInt(info);
                        break;
                    case "clip":
                        clip = Boolean.parseBoolean(info);
                        break;
                    case "optic":
                        optic = Boolean.parseBoolean(info);
                        break;
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if(modelName != null && ! modelName.isEmpty()
                    && handy != null && handy != 0
                    && origin != null && !origin.isEmpty()
                    && material != null && !material.isEmpty()
                    && range != null && range != 0
                    && scope != null && scope != 0
                    && clip != null && optic != null){
                Gun gun = new Gun(modelName, handy, origin, material, range, scope, clip, optic);
                guns.add(gun);
                modelName=null;
                handy=null;
                origin=null;
                material=null;
                range=null;
                scope=null;
                clip=null;
                optic=null;
            }
        }
    }

    private void sortGuns(){
        Comparator<Gun>gunComparator = new Comparator<Gun>() {
            @Override
            public int compare(Gun o1, Gun o2) {
                return o2.getTtc().getScope().compareTo(o1.getTtc().getScope());
            }
        };
        guns.sort(gunComparator);
    }

    public final void show(){
        sortGuns();
        guns.forEach(System.out::println);
    }

    public final void write() throws ParserConfigurationException {
        new Writer().writeToXML(guns,"SAXGuns.xml");
    }

}
