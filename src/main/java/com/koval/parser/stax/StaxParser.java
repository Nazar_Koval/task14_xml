package com.koval.parser.stax;

import com.koval.model.gun.Gun;
import com.koval.writer.Writer;
import org.apache.logging.log4j.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Comparator;
import static com.koval.validator.Validator.*;

public class StaxParser {

    private static ArrayList<Gun>guns = new ArrayList<>();
    private static Gun gun;
    private static Logger logger = LogManager.getLogger(StaxParser.class);
    private static XMLInputFactory xmlInputFactory;
    private static XMLStreamReader xmlStreamReader;

    public StaxParser() throws Exception {

        if(validate("src/main/resources/gunsXSD.xsd",
                "src/main/resources/guns.xml")){
            xmlInputFactory = XMLInputFactory.newInstance();
            xmlStreamReader = xmlInputFactory.createXMLStreamReader(
                    new FileInputStream("src/main/resources/guns.xml"));
            getInfo();
        }else logger.trace("Failure while parsing");
    }

    private void getInfo()throws Exception{

        while (xmlStreamReader.hasNext()){
            int event = xmlStreamReader.next();

            if(event == XMLEvent.START_ELEMENT){
                String var = xmlStreamReader.getLocalName().replace("\n","").trim();

                if(var.equals("modelName")){
                    gun = new Gun();
                    gun.setModelName(xmlStreamReader.getElementText().trim());
                    xmlStreamReader.next();
                }else if(var.equals("handy")){
                    gun.setHandy(Integer.parseInt(xmlStreamReader.getElementText().trim()));
                    xmlStreamReader.next();
                }else if(var.equals("origin")) {
                    gun.setOrigin(var);
                    xmlStreamReader.next();
                }else if(var.equals("material")){
                    gun.setMaterial(xmlStreamReader.getElementText().trim());
                    xmlStreamReader.next();
                }else if(var.equals("range")){
                    gun.getTtc().setRange(Integer.parseInt(xmlStreamReader.getElementText().trim()));
                    xmlStreamReader.next();
                }else if(var.equals("scope")){
                    gun.getTtc().setScope(Integer.parseInt(xmlStreamReader.getElementText().trim()));
                    xmlStreamReader.next();
                }else if(var.equals("clip")){
                    gun.getTtc().setClip(Boolean.parseBoolean(xmlStreamReader.getElementText().trim()));
                    xmlStreamReader.next();
                }else if(var.equals("optic")){
                    gun.getTtc().setOptic(Boolean.parseBoolean(xmlStreamReader.getElementText().trim()));
                    guns.add(gun);
                    xmlStreamReader.next();
                }
            }
        }
    }

    private void sortGuns(){
        Comparator<Gun> gunComparator = new Comparator<Gun>() {
            @Override
            public int compare(Gun o1, Gun o2) {
                return o2.getMaterial().compareTo(o1.getMaterial());
            }
        };
        guns.sort(gunComparator);
    }

    public final void show(){
        sortGuns();
        guns.forEach(System.out::println);
    }

    public final void write() throws ParserConfigurationException {
        new Writer().writeToXML(guns,"StAXGuns.xml");
    }
}
