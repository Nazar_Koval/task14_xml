package com.koval.parser.dom;

import com.koval.model.gun.Gun;
import com.koval.writer.Writer;
import edu.emory.mathcs.backport.java.util.Collections;
import org.apache.logging.log4j.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import static com.koval.validator.Validator.validate;

public class DomParser {

    private static Logger logger = LogManager.getLogger(DomParser.class);
    private static ArrayList<Gun> guns = new ArrayList<>();
    private Gun gun;

    public DomParser()throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse("src/main/resources/guns.xml");

        if(validate("src/main/resources/gunsXSD.xsd",
                "src/main/resources/guns.xml")){
                getInfo(document.getDocumentElement().getChildNodes());
        }else {
            logger.trace("Failure during validation.");
        }
    }

    private void getInfo(final NodeList nodeList){

        for (int i = 0; i < nodeList.getLength(); i++) {

            if(nodeList.item(i) instanceof Element){

                if(!nodeList.item(i).getTextContent().replace("\n","").trim().isEmpty()
                    && !((Text)nodeList.item(i).getFirstChild()).getData()
                        .replace("\n","").trim().isEmpty()){

                    String var = ((Text)nodeList.item(i).getFirstChild()).getData().trim();

                    switch (((Element) nodeList.item(i)).getTagName()) {
                        case "modelName":
                            gun = new Gun();
                            gun.setModelName(var);
                            break;
                        case "handy":
                            gun.setHandy(Integer.parseInt(var));
                            break;
                        case "origin":
                            gun.setOrigin(var);
                            break;
                        case "material":
                            gun.setMaterial(var);
                            break;
                        case "range":
                            gun.getTtc().setRange(Integer.parseInt(var));
                            break;
                        case "scope":
                            gun.getTtc().setScope(Integer.parseInt(var));
                            break;
                        case "clip":
                            gun.getTtc().setClip(Boolean.parseBoolean(var));
                            break;
                        case "optic":
                            gun.getTtc().setOptic(Boolean.parseBoolean(var));
                            guns.add(gun);
                            break;
                    }
                }

                if(nodeList.item(i).hasChildNodes()){
                    getInfo(nodeList.item(i).getChildNodes());
                }
            }
        }
    }

    private void sortGuns(){
        Comparator<Gun>gunComparator = new Comparator<Gun>() {
            @Override
            public int compare(final Gun o1, final Gun o2) {
                return o1.getTtc().getRange().compareTo(o2.getTtc().getRange());
            }
        };
        Collections.sort(guns,gunComparator);
    }

    public final void show(){
        sortGuns();
        guns.forEach(System.out::println);
    }

    public final void write() throws ParserConfigurationException {
        new Writer().writeToXML(guns,"DOMGuns.xml");
    }
}
