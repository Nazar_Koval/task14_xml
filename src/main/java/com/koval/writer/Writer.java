package com.koval.writer;

import com.koval.model.gun.Gun;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.ArrayList;

public class Writer {

    private static ArrayList<Gun> guns = new ArrayList<>();

    private Document createDoc() throws ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        DOMImplementation domImplementation = documentBuilder.getDOMImplementation();
        return domImplementation.createDocument(null, "guns", null);
    }

    private void transform(final Document doc, final StreamResult res) {
        DOMSource domSource = new DOMSource(doc);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(domSource, res);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    private void fill(final Document doc, final Element root) {
        for (int i = 0; i < guns.size(); i++) {
            Element gun = doc.createElement("gun");
            gun.setAttribute("gunNo", String.valueOf(i + 1));
            Element name = doc.createElement("name");
            Node textName = doc.createTextNode(guns.get(i).getModelName());
            name.appendChild(textName);
            Element handy = doc.createElement("handy");
            Node handyText = doc.createTextNode(String.valueOf(guns.get(i).getHandy()));
            handy.appendChild(handyText);
            Element origin = doc.createElement("origin");
            Node originText = doc.createTextNode(guns.get(i).getOrigin());
            origin.appendChild(originText);
            Element material = doc.createElement("material");
            Node materialText = doc.createTextNode(guns.get(i).getMaterial());
            material.appendChild(materialText);
            Element ttc = doc.createElement("ttc");
            Element range = doc.createElement("range");
            Node rangeText = doc.createTextNode(String.valueOf(guns.get(i).getTtc().getRange()));
            range.appendChild(rangeText);
            Element scope = doc.createElement("scope");
            Node scopeText = doc.createTextNode(String.valueOf(guns.get(i).getTtc().getScope()));
            scope.appendChild(scopeText);
            Element clip = doc.createElement("clip");
            Node clipText = doc.createTextNode(String.valueOf(guns.get(i).getTtc().isClip()));
            clip.appendChild(clipText);
            Element optic = doc.createElement("optic");
            Node opticText = doc.createTextNode(String.valueOf(guns.get(i).getTtc().isOptic()));
            optic.appendChild(opticText);
            ttc.appendChild(range);
            ttc.appendChild(scope);
            ttc.appendChild(clip);
            ttc.appendChild(optic);
            gun.appendChild(name);
            gun.appendChild(handy);
            gun.appendChild(origin);
            gun.appendChild(material);
            gun.appendChild(ttc);
            root.appendChild(gun);
        }
    }

    public final void writeToXML(final ArrayList<Gun> gunArrayList, final String path) throws ParserConfigurationException {
        guns = gunArrayList;
        Document doc = createDoc();
        Element root = doc.getDocumentElement();
        fill(doc, root);
        Node info = doc.createProcessingInstruction("xml-stylesheet",
                "type=\"text/xsl\" href=\"gunsXSLT.xsl\"");
        doc.insertBefore(info, root);
        StreamResult streamResult = new StreamResult(path);
        transform(doc, streamResult);
    }
}
