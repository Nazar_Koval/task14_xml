package com.koval;

import com.koval.parser.dom.DomParser;
import com.koval.parser.sax.SaxParser;
import com.koval.parser.stax.StaxParser;
import org.apache.logging.log4j.*;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args)throws Exception {
        logger.trace("~~~~~~~~~~~~~DOM~~~~~~~~~~~~~\n");
        DomParser domParser = new DomParser();
        domParser.show();
        domParser.write();
        logger.trace("\n~~~~~~~~~~~~~SAX~~~~~~~~~~~~~\n");
        SaxParser saxParser = new SaxParser();
        saxParser.show();
        saxParser.write();
        logger.trace("\n~~~~~~~~~~~~~STAX~~~~~~~~~~~~~\n");
        StaxParser staxParser = new StaxParser();
        staxParser.show();
        staxParser.write();
    }
}
